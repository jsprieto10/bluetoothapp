//
//  AttendenceViewController.swift
//  bluethootApp
//
//  Created by Sebastian Prieto on 8/17/19.
//  Copyright © 2019 Sebastian Prieto. All rights reserved.
//

import UIKit

class AttendenceViewController: UIViewController {

    @IBOutlet weak var button: UIButton!
    
    @IBOutlet weak var tableview: UITableView!
    
    @IBOutlet weak var tableViewVagos: UITableView!
    
    @IBOutlet weak var check: UIButton!
    
    @IBOutlet weak var csv: UIButton!
    
    var dates = ["5-7-2019", "6-7-2019","9-7-2019"]
    
    var students = [String:String]()
    
    var vagos = [String:String]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableview.isHidden = true
        tableViewVagos.isHidden = true
        
        let url = URL(string: "https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/studentsList?pageSize=100")!
        
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                print("error: \(error)")
                
            }
            else {
                do {
                    let json = try JSONSerialization.jsonObject(with: data!, options: [])
                    
                    if let dictionary = json as? [String: Any] {
                    
                        if let documents = dictionary["documents"] as? [[String:Any]] {
                            for document in documents{
                                
                                if let fields = document["fields"] as? [String:Any]{
                                    
                                    var name:String = ""
                                    var code:String = ""
                                    
                                    if let nameDi = fields["name"] as? [String:Any]{
                                        
                                        if let val = nameDi["stringValue"] as? String{
                                            name = val
                                        }
                                        
                                    }
                                    if let codeDi = fields["code"] as? [String:Any]{
                                        
                                        if let val = codeDi["stringValue"] as? String{
                                            code = val
                                        }
                                        
                                    }
                                    
                                    self.students[code] = name as String
                                    
                                    
                                }
                            }
                            
                        }
                        
                    }
                    
                    DispatchQueue.main.async() {
                        self.check.isEnabled = true
                    }
                    
                    
                } catch {
                    print("JSON error: \(error.localizedDescription)")
                    
                }
            }
        }
        task.resume()

        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func onClickDrop(_ sender: UIButton) {
        
        
        UIView.animate(withDuration: 0.3) {
            self.tableview.isHidden = !self.tableview.isHidden
        }
        
    }
    func showToast(message : String) {
        
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 75, y: self.view.frame.size.height-100, width: 200, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    
    @IBAction func checkList(_ sender: UIButton) {
        
        let selected = button.currentTitle!
        
        if selected == "Escoja una fecha"{
            
            showToast(message: "Seleccione una fecha")
            
        }
        
        else{
            
            self.vagos = self.students
            
            let url = "https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/attendance/\(selected)/students?pageSize=100"
            
            print(url)
            
            let task = URLSession.shared.dataTask(with: URL(string:url)!) { (data, response, error) in
                if let error = error {
                    print("error: \(error)")
                    
                }
                else {
                    do {
                        let json = try JSONSerialization.jsonObject(with: data!, options: [])
                        
                        if let dictionary = json as? [String: Any] {
                            
                            if let documents = dictionary["documents"] as? [[String:Any]] {
                                for document in documents{
                                    
                                    if let fields = document["fields"] as? [String:Any]{
                                        
                                        
                                        if let codeDi = fields["code"] as? [String:Any]{
                                            
                                            if let code = codeDi["stringValue"] as? String{
                                                
                                                self.vagos.removeValue(forKey: code)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        
                        DispatchQueue.main.async() {
                            self.tableViewVagos.reloadData()
                            self.tableViewVagos.isHidden = false
                            self.alertE(mensaje:"Se encontraron \(self.vagos.count) vagos")
                            self.csv.isEnabled=true
                        
                        }
                    } catch {
                        print("JSON error: \(error.localizedDescription)")
                        
                    }
                }
            }
            task.resume()

            
            
            
        }
        
    }
    
    func alertE(mensaje: String)
    {
        let alert = UIAlertController(title: "Mensaje", message: mensaje, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        
        self.present(alert, animated: true)
    }
    
    
    @IBAction func generateCSV(_ sender: UIButton) {
        
        var csvString = "\("Codigo"),\("Nombre")\n\n"
        for (codigo, nombre) in vagos{
            csvString = csvString.appending("\(codigo),\(nombre)\n")
        }
        
        let fileManager = FileManager.default
        do {
            let path = try fileManager.url(for: .documentDirectory, in: .allDomainsMask, appropriateFor: nil, create: false)
            print(path)
            let fileURL = path.appendingPathComponent("vagos-\(self.button.currentTitle!).csv")
            try csvString.write(to: fileURL, atomically: true, encoding: .utf8)
            alertE(mensaje: "Se ha generado el csv")
            let vc = UIActivityViewController(activityItems: [path], applicationActivities: [])
            vc.excludedActivityTypes = [
                UIActivity.ActivityType.assignToContact,
                UIActivity.ActivityType.saveToCameraRoll,
                UIActivity.ActivityType.postToFlickr,
                UIActivity.ActivityType.postToVimeo,
                UIActivity.ActivityType.postToTencentWeibo,
                UIActivity.ActivityType.postToTwitter,
                UIActivity.ActivityType.postToFacebook,
                UIActivity.ActivityType.openInIBooks
            ]
            present(vc, animated: true, completion: nil)
        } catch {
            print("error creating file")
        }
        
    }
    
}

extension AttendenceViewController: UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == self.tableview{
            return dates.count
        }
        else{
            
            let listaVagos=Array(self.vagos.values)
            
            print(listaVagos.count)
            return listaVagos.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        if tableView == self.tableview{
        
            cell.textLabel?.text = dates[indexPath.row]
        }
        else{
            let listaVagos=Array(self.vagos.values)
            cell.textLabel?.text = listaVagos[indexPath.row]
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == self.tableview{
        
        button.setTitle(dates[indexPath.row], for: .normal)
        tableView.isHidden = true
            
        }
        
    }
    
}
