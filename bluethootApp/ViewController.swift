//
//  ViewController.swift
//  bluethootApp
//
//  Created by Sebastian Prieto on 8/10/19.
//  Copyright © 2019 Sebastian Prieto. All rights reserved.
//

import UIKit
import CoreBluetooth

class ViewController: UIViewController,CBPeripheralManagerDelegate {
    
    
    func peripheralManagerDidUpdateState(_ peripheral: CBPeripheralManager) {
        
        switch (peripheral.state) {
        case .poweredOn:
            print("Current Bluetooth State:  PoweredOn")
            let advertisementData = [CBAdvertisementDataLocalNameKey: "jsprieto10"]
            peripheralManager?.startAdvertising(advertisementData)
            self.button.isEnabled = true

            break;
        case .poweredOff:
            print("Current Bluetooth State:  PoweredOff")
            self.button.isEnabled = false
            break;
        case .resetting:
            print("Current Bluetooth State:  Resetting")
            break;
        case .unauthorized:
            print("Current Bluetooth State:  Unauthorized")
        case .unknown:
            print("Current Bluetooth State:  Unknown")
            break;
        case .unsupported:
            print("Current Bluetooth State:  Unsupported")
            break;
        @unknown default:
            print("error")
        }
        
    }
    
    
    
    func alertE()
    {
        let alert = UIAlertController(title: "Mensaje", message: "Ha terminado la busqueda", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        
        self.present(alert, animated: true)
    }
    
    var activityIndicator:UIActivityIndicatorView = UIActivityIndicatorView()
    var labelCorrect = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 21))
    
    
    @IBOutlet weak var button: UIButton!
    
    var peripheralManager: CBPeripheralManager!
    

    @IBAction func answer(_ sender: Any)
    {
        labelCorrect.center = self.view.center
        labelCorrect.textAlignment = .center
        
        
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.style = UIActivityIndicatorView.Style.gray
        activityIndicator.startAnimating()
        
        self.view.addSubview(activityIndicator)
        
        
        let date = Date()
        let calendarDate = Calendar.current.dateComponents([.day, .year, .month], from: date)
        
        
        let year = calendarDate.year!
        let month = calendarDate.month!-1
        let day = calendarDate.day!
        
        
        
        let url = URL(string: "https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/attendance/\(day)-\(month)-\(year)/students/201426358")!
        
        
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                print("error: \(error)")
                
            }
            else {
                
                if let response = response as? HTTPURLResponse {
                    
                    if response.statusCode == 200{
                        self.addLabelView(text: "!Ya estas en la lista!")
                        self.peripheralManager?.stopAdvertising()
                    }
                    else
                    {
                        self.addLabelView(text: "Ups, hoy no apareces")
                    }
                    
                
                }
                
            }
        }
        task.resume()
        
    }
    
    
    func addLabelView(text:String)
    {
        DispatchQueue.main.async() {
            self.alertE()
            self.activityIndicator.removeFromSuperview()
            
            self.labelCorrect.text = text
            self.view.addSubview(self.labelCorrect)
        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        peripheralManager = CBPeripheralManager(delegate: self, queue: nil)
        
        
    }
    
    
}



